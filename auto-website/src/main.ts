import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import setupElementPlus from "./plugins/element-plus"
import {setupLang} from "./plugins/lang"
import {setupResizeObserver} from "./plugins/extend"
import {setupSvgIcon} from "./plugins/svg-icon"
const app = createApp(App)
import JsonViewer from "vue3-json-viewer"
import "./assets/style/fonts.css"

setupElementPlus(app)
setupLang(app)
setupResizeObserver()
setupSvgIcon(app)

app.use(router)
app.use(JsonViewer)
app.mount('#app')
