import {createRouter, createWebHashHistory, RouteRecordRaw} from 'vue-router'
import Kommon from './kommon'
import SystemManage from "./system-manage"

const routes: Array<RouteRecordRaw> = [
  ...Kommon,
  ...SystemManage
]

const router = createRouter({
  history: createWebHashHistory(process.env.BASE_URL),
  routes
})

export default router
