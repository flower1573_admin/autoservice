import SystemManage from '../views/system-manage/index.vue'


import LogRecords from '../views/system-manage/log-manage/log-records/index.vue'
import AuditLog from '../views/system-manage/log-manage/audit-log/index.vue'

import ScheduledManage from '../views/system-manage/scheduled-manage/index.vue'

const system = [
  {
    path: '/system-manage',
    name: 'system-manage',
    component: SystemManage,
    children: [
      {
        path: 'log-manage/log-records',
        name: 'log-records',
        component: LogRecords
      },
      {
        path: 'log-manage/audit-log',
        name: 'audit-log',
        component: AuditLog
      },
      {
        path: 'scheduled-manage',
        name: 'scheduled-manage',
        component: ScheduledManage
      }
    ]
  }
]

export default system
