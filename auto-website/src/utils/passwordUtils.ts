/**
 * 长度校验
 */
export const validLength = (value: any, min: any, max: any) => {
  const regexString = `^(.){${min},${max}}$`;
  const regExp = new RegExp(regexString);

  return regExp.test(value)
}

/**
 * 大小写字母校验
 *
 * @param value
 * @param min
 * @param max
 */
export const validLetterLen = (value: any, lowerLetterLen: any, upperLetterLen: any) => {
  const lowercaseRegex = /[a-z]/g;
  const uppercaseRegex = /[A-Z]/g;

  const lowercaseCount = (value.match(lowercaseRegex) || []).length;
  const uppercaseCount = (value.match(uppercaseRegex) || []).length;

  return lowercaseCount >= lowerLetterLen && uppercaseCount >= upperLetterLen;
}

/**
 * 校验特殊字符长度
 * @param value
 * @param lowerLetterLen
 * @param upperLetterLen
 */
export const validSpecialLen = (value: any, minLen: any) => {
  const regex = new RegExp(`[^A-Za-z0-9]`, 'g');
  const specialCharactersCount = (value.match(regex) || []).length;
  return specialCharactersCount >= minLen;
}

/**
 * 校验连续相同字符
 * @param value
 * @param lowerLetterLen
 * @param upperLetterLen
 */
export const validSameLen = (value: any, maxLen: any) => {
  const regex = new RegExp(`(.)\\1{${maxLen - 1},}`, 'g');
  return !regex.test(value)
}