import {h} from 'vue'

export const setTableIndex = (page: any, size: any, index: any) => {
  return (page - 1) * size + index + 1;
}

export const renderHeader = (event: any) => {
  return h(
    "div",
    {
      slot: "content",
      class: "table-header-flex",
    },
    [
      h(
        "el-tooltip",
        {
          props: {
            placement: "top",
          },
        },
        [
          h(
            "span",
            {
              slot: "content",
              style: {
                whiteSpace: "normal",
                overflow: "hidden",
                "text-overflow": "ellipsis",
                "overflow-wrap": "break-word",
                "white-space": "nowrap",
                "width": "70px",
                display: "inline-block",
              },
            },
            event.column.label
          ),
        ]
      ),
    ]
  );
}