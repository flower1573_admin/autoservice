import axios from 'axios'
import  AuthInterceptor from "../components/components-auth-interceptor"
import router from '../router'

const HttpRequest = axios.create({
  baseURL: "http://192.168.2.7:31984"
});

const executeInterceptor = (event: any) => {
  const validUrlList = ["/", "/login-in-view", "/register-view", "/resetpwd-view"]
 /* if (event.data.code == 401 && !validUrlList.includes(router.currentRoute.value.path)) {
    AuthInterceptor().then(() => {
      setTimeout(() => {
        router.push({
          path: "/login-in-view"
        })
      }, 500)
    }).catch(() => {
      setTimeout(() => {
        router.push({
          path: "/register-view"
        })
      }, 500)
    })
  }*/
}

const setConfig = (event: any) => {
  const  pagePath = router.currentRoute.value.path

  const _config = {
    ...event,
  } as any
  _config.headers["x-uem-page"] = pagePath

  return _config
}

HttpRequest.interceptors.request.use(
  config => {
    const token = localStorage.getItem('token');
    if (token) {
      config.headers.Authorization = token;
    }
    const _config  =setConfig(config)
    return _config;
  },
  error => Promise.reject(error)
)

HttpRequest.interceptors.response.use(function (response) {
  executeInterceptor(response)
  return Promise.resolve(response.data);
}, function (error) {
  return Promise.reject(error);
});

export default HttpRequest
