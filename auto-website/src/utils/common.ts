/**
 * 数组模糊搜索
 *
 * @param dataList 原始数组
 * @param keyword 搜索关键字
 * @param dataListKey 数组对象key
 */
export const fuzzySearchArray = (dataList: any,  keyword: any, dataListKey?: any) => {
  try {
    if (Array.isArray(dataList)) {
      return dataList.filter((item: any) => {
        let itemLowerCase = item.toString().toLowerCase();
        if (dataListKey) {
          itemLowerCase = item[dataListKey].toString().toLowerCase();
        }
        const keywordLowerCase = keyword.toString().toLowerCase();
        return itemLowerCase.includes(keywordLowerCase);
      });
    }
    return [];
  } catch (e) {
    return []
  }
}