export const validIpv4Url = (url: string) => {
  const pattern = /^(https?:\/\/)(?:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})(?::\d{1,5})?(?:[/?#]\S*)?$/;
  return pattern.test(url);
}

export const validDomainUrl = (url: string) => {
  const pattern = /^(https?:\/\/)([a-zA-Z0-9-]+\.)+[a-zA-Z]{2,6}(\/\S*)?$/;
  return pattern.test(url);
}

export const validateEmail = (email: any) => {
  // 正则表达式用于匹配邮箱格式
  const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return emailPattern.test(email);
}

export const validatePhoneNumber = (phoneNumber: any) => {
  const regex = /^1[3456789]\d{9}$/;
  return regex.test(phoneNumber);
}