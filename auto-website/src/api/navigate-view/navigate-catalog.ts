import HttpRequest from "../../utils/request"

export const getNavigateCatalogList = (params: any) => {
  return HttpRequest.post("/navigate/catalog/getNavigateCatalogList", params)
}

export const getAllNavigateCatalogList = () => {
  return HttpRequest.post("/navigate/catalog/getAllNavigateCatalogList")
}

export const createNavigateCatalog = (params: any) => {
  return HttpRequest.post("/navigate/catalog/createNavigateCatalog", params)
}

export const updateNavigateCatalog = (params: any) => {
  return HttpRequest.post("/navigate/catalog/updateNavigateCatalog", params)
}

export const deleteNavigateCatalog = (params: any) => {
  return HttpRequest.post("/navigate/catalog/deleteNavigateCatalog", params)
}