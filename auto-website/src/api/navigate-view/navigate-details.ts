import HttpRequest from "../../utils/request"


export const getNavigateDetailsList = (params: any) => {
  return HttpRequest.post("/navigate/details/getNavigateDetailsList", params)
}

export const createNavigateDetails = (params: any) => {
  return HttpRequest.post("/navigate/catalog/createNavigateDetails", params)
}

export const updateNavigateDetails = (params: any) => {
  return HttpRequest.post("/navigate/catalog/updateNavigateDetails", params)
}

export const deleteNavigateDetails = (params: any) => {
  return HttpRequest.post("/navigate/catalog/deleteNavigateDetails", params)
}