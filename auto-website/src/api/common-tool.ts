import HttpRequest from "../utils/request"

/**
 * 外网发包-注册账号
 *
 * @param params
 */
export const stoneRegisterAccount = (params: any) => {
  return HttpRequest.post("/user/stoneRegisterAccount", params)
}

/**
 * 外网发包-校验账号
 *
 * @param params
 */
export const stoneCheckAccount = (params: any) => {
  return HttpRequest.post("/user/stoneCheckAccount", params)
}

/**
 * 外网发包-获取token
 *
 * @param params
 */
export const stoneGetToken = (params: any) => {
  return HttpRequest.post("/user/stoneGetToken", params)
}

/**
 * 外网发包-文件上传
 *
 * @param params
 */
export const stoneUploadFIle = (params: any) => {
  return HttpRequest.post("/user/stoneUploadFIle", params)
}


