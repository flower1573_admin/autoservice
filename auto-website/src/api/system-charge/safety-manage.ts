import HttpRequest from "../../utils/request"

export const getAccountPolicy = () => {
  return HttpRequest.post("/accountPolicy/getPolicy")
}

export const updateAccountPolicy = (params: any) => {
  return HttpRequest.post("/accountPolicy/updatePolicy", params)
}

export const getPasswordPolicy = () => {
  return HttpRequest.post("/passwordPolicy/getPolicy")
}

export const updatePasswordPolicy = (params: any) => {
  return HttpRequest.post("/passwordPolicy/updatePolicy", params)
}

export const getLoginAddressPolicy = (params: any) => {
  return HttpRequest.post("/loginAddressPolicy/getPolicy", params)
}

export const updateLoginAddressPolicy = (params: any) => {
  return HttpRequest.post("/loginAddressPolicy/updatePolicy", params)
}

export const createLoginAddressPolicy = (params: any) => {
  return HttpRequest.post("/loginAddressPolicy/createLoginAddressPolicy", params)
}

export const getLoginTimePolicy = (params: any) => {
  return HttpRequest.post("/loginTimePolicy/getPolicy", params)
}

export const updateLoginTimePolicy = (params: any) => {
  return HttpRequest.post("/loginTimePolicy/updatePolicy", params)
}