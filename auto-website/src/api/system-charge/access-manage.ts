import HttpRequest from "../../utils/request"

export const getUemList = (params: any) => {
  return HttpRequest.post("/uem/getUemList", params)
}

export const getUemAnalyze = () => {
  return HttpRequest.post("/uemAnalyze/getUemAnalyze")
}