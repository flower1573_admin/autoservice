import HttpRequest from "../../utils/request"

export const getScheduleList = (params: any) => {
  return HttpRequest.post("/schedule/getScheduleList", params)
}

export const createSchedule = (params: any) => {
  return HttpRequest.post("/schedule/createSchedule", params)
}

export const updateSchedule = (params: any) => {
  return HttpRequest.post("/schedule/updateSchedule", params)
}

export const updateScheduleStatus = (params: any) => {
  return HttpRequest.post("/schedule/updateScheduleStatus", params)
}

export const deleteSchedule = (params: any) => {
  return HttpRequest.post("/schedule/deleteSchedule", params)
}
