import HttpRequest from "../../utils/request"

export const getUserList = (params: any) => {
  return HttpRequest.post("/user/getUserList", params)
}

export const getUser = (params: any) => {
  return HttpRequest.post("/user/getUser", params)
}

export const createUser = (params: any) => {
  return HttpRequest.post("/user/createUser", params)
}

export const deleteUser = (params: any) => {
  return HttpRequest.post("/user/deleteUser", params)
}
