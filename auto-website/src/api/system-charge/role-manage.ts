import HttpRequest from "../../utils/request"

export const getRoleList = (params: any) => {
  return HttpRequest.post("/role/getRoleList", params)
}

export const getRoleInfo = (params: any) => {
  return HttpRequest.post("/role/getRole", params)
}

export const getRoleListByUser = (params: any) => {
  return HttpRequest.post("/role/getRoleListByUser", params)
}

export const createRole = (params: any) => {
  return HttpRequest.post("/role/createRole", params)
}

export const updateRole = (params: any) => {
  return HttpRequest.post("/role/updateRole", params)
}

export const deleteRole = (params: any) => {
  return HttpRequest.post("/role/deleteRole", params)
}

export const getMenuList = () => {
  return HttpRequest.post("/menu/getMenuList")
}

export const getMenuListByRole = (params: any) => {
  return HttpRequest.post("/menu/getMenuListByRole", params)
}