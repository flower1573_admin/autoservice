import HttpRequest from "../../utils/request"
import axios from "axios"

export const getTableList = (params: any) => {
  return HttpRequest.post("/system/log/getLogList", params)
}


export const exportLogList = (params: any) => {
  const config = {
    responseType: "blob",
  } as any
  return axios.post('http://127.0.0.1:31984/system/log/exportLogList', params, config)
}

export const getUserInfo = () => {
  return HttpRequest.get('http://127.0.0.1:31984/admin/menuList')
}

export const loginAccount = (params: any) => {
  return axios.post('http://127.0.0.1:31984/user/login', params)
}

export const getAuditLogList = (params: any) => {
  return HttpRequest.post("/auditLog/getAuditLogList", params)
}

export const getOperateLogConfig = () => {
  return HttpRequest.post("/operateLogConfig/getConfig")
}

export const updateOperateLogConfig = (params: any) => {
  return HttpRequest.post("/operateLogConfig/updateConfig", params)
}