import HttpRequest from "../../utils/request"

export const getServerInfo = () => {
  return HttpRequest.post("/server/getServerInfo")
}