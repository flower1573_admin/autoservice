import HttpRequest from "../../utils/request"
import axios from "axios"

export const loginInAccount = (params: any) => {
  return HttpRequest.post("/user/loginIn", params)
}

export const loginOutAccount = () => {
  return HttpRequest.get("/logout")
}

export const getAccountInfo = () => {
  return HttpRequest.post("user/getUserInfo")
}

export const updateUser = (params: any) => {
  return HttpRequest.post("user/updateUserInfo", params)
}