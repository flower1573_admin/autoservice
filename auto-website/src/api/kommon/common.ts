import HttpRequest from "../../utils/request"

export const registerAccount = (params: any) => {
  return HttpRequest.post("/user/registerUser", params)
}

export const resetPassword = (params: any) => {
  return HttpRequest.post("/user/resetPassword", params)
}

export const getSafetyPolicy = () => {
  return HttpRequest.post("/safetyPolicy/getSafetyPolicy")
}