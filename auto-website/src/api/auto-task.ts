import HttpRequest from "../utils/request"

export const getTaskList = (params: any) => {
  return HttpRequest.post("/autoTask/getTaskList", params)
}

export const createTask = (params: any) => {
  return HttpRequest.post("/autoTask/createTask", params)
}

export const configTask = (params: any) => {
  return HttpRequest.post("/autoTask/configTask", params)
}

export const executeTask = (params: any) => {
  return HttpRequest.post("/autoTask/executeTask", params)
}

export const deleteTask = (params: any) => {
  return HttpRequest.post("/autoTask/deleteTask", params)
}