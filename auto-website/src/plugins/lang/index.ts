import { createI18n } from 'vue-i18n'
import i18n from "../../assets/i18n"

const zhMessages = {} as any;
const deMessages = {}  as any;

Object.entries(i18n.zh).map((prev: any)=>{
  Object.keys(prev[1]).forEach(key => {
    const messageKey = `${prev[0]}.${key}`
    zhMessages[messageKey] = prev[1][key]
  });
});

Object.entries(i18n.de).map((prev: any)=>{
  Object.keys(prev[1]).forEach(key => {
    const messageKey = `${prev[0]}.${key}`
    deMessages[messageKey] = prev[1][key]
  });
});

const messages = {
  zh: zhMessages,
  de: deMessages
}

const currentLang = localStorage.getItem("mml_lang") || "de"
export const _i18n = createI18n({
  legacy: false,
  locale:  currentLang,
  messages: messages,
  fallbackLocale: currentLang,
})

function initLang() {
  const currentLang = localStorage.getItem("mml_lang")
  if (!currentLang){
    localStorage.setItem("mml_lang", "de")
  }
}

export const setupLang = (app: any) => {
  initLang()
  app.use(_i18n)
}