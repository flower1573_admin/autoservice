export const setupResizeObserver = () => {

  const debounce = (callback: Function, delay: number) => {
    let tid: any
    return function () {
      const ctx = self
      tid && clearTimeout(tid);
      tid = setTimeout(() => {
        callback.apply(ctx, arguments)
      }, delay)
    }
  }

  const _ = (window as any).ResizeObserver;
  (window as any).ResizeObserver = class ResizeObserver extends _ {
    constructor(callback: any) {
      callback = debounce(callback, 20)
      super(callback);
    }
  }
}