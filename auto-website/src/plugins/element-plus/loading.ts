import {ElLoading} from "element-plus";

let loadingInstance: ReturnType < typeof ElLoading.service > ;

const startLoading = () => {
    loadingInstance = ElLoading.service({
        fullscreen: true,
        lock: true,
        text: "加载中...",
        background: "#FFFFFF"
    });
};

const endLoading = () => {
    loadingInstance.close();
};

let needLoadingRequestCount = 0;
export const showLoading = () => {
    if (needLoadingRequestCount === 0) {
        startLoading();
    }
    needLoadingRequestCount++;
};

export const closeLoading = () => {
    if (needLoadingRequestCount <= 0) return;
    needLoadingRequestCount--;
    if (needLoadingRequestCount === 0) {
        setTimeout(() => {
          endLoading();
        }, 1000 * 0.2)
    }
};