import { ElNotification } from 'element-plus'

const showNotification = (type: string, message: string) => {
    let _title = "通知"
    switch (type) {
        case "success":
            _title = "成功"
            break;
        case "warning":
            _title = "警告"
            break;
        case "error":
            _title = "错误"
            break;
    }
    ElNotification({
        title: _title,
        message: message,
        type: type as any,
    })
}

export const showInfoNotification = (message: string) => showNotification("info", message)

export const showSuccessNotification = (message: string) => showNotification("success", message)

export const showWarningNotification = (message: string) => showNotification("warning", message)

export const showErrorNotification = (type: string, message: string) => showNotification("error", message)
