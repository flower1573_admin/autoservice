import {
    showInfoNotification,
    showSuccessNotification,
    showWarningNotification,
    showErrorNotification
} from './notification'
import {showLoading, closeLoading} from './loading'

import 'element-plus/dist/index.css';
import './theme.scss';

const setupElementPlus = (app: any) => {
    app.config.globalProperties.$InfoNotification = showInfoNotification
    app.config.globalProperties.$SuccessNotification = showSuccessNotification
    app.config.globalProperties.$WarningNotification = showWarningNotification
    app.config.globalProperties.$ErrorNotification = showErrorNotification

    app.config.globalProperties.$ShowLoading = showLoading
    app.config.globalProperties.$CloseLoading = closeLoading
}

export default setupElementPlus