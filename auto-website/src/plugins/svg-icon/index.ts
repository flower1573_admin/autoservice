import svgIcon from '../../components/components-svg/index.vue'

const requireAll = (requireContext: any) => requireContext.keys().map(requireContext)
const svgRequire = require.context('../../assets/svg', false, /\.svg$/)
requireAll(svgRequire);

export const setupSvgIcon = (app: any) => {
  app.component('svg-icon', svgIcon)
}

