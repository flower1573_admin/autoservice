import commonZh from "./common/zh"
import commonDe from "./common/de"

import systemZh from "./system/zh"
import systemDe from "./system/de"

import navigateZh from "./navigate/zh"
import navigateDe from "./navigate/de"

const i18n = {
  zh: {
    ...commonZh,
    ...systemZh,
    ...navigateZh
  },
  de: {
    ...commonDe,
    ...systemDe,
    ...navigateDe
  }
}

export default i18n