export default {
  system: {
    "overview": "系统总览",
    "auth.manage": "权限管理",
    "log.manage": "日志管理",
    "role.manage": "角色管理",
    "user.manage": "用户管理",
    "log.config": "日志配置",
    "log.records": "日志记录",
    "scheduledManage": "定时任务管理",

    "auth.account": "账号",
    "auth.account.placeholder": "请输入账号",
    "auth.password": "密码",
    "auth.password.placeholder": "请输入密码",

    "configRole": "配置角色",
    "authAll": "全部权限",
    "authChecked": "已选权限",

    "configUser": "配置用户",
    "roleAll": "全部角色",
    "roleChecked": "已选角色",

    "safetyPolicy": "安全策略",
    "accountPolicy": "账号策略",
    "passwordPolicy": "密码策略",
    "ipPolicy": "登录IP策略",
    "timePolicy": "登陆时间控制",

    "account.minLen": "账号最小字符",
    "account.expireTime": "账号到期前提示（天）",

    "password.maxLen": "最多字符",
    "password.minLen": "最少字符",
    "password.minLetter": "最少字母个数",
    "password.minUpperLetter": "最少大写字符个数",
    "password.minLowerLetter": "最少小写字符个数",
    "password.minSpecialLetter": "最少特殊字符个数",
    "password.increactLetter.enable": "密码不能是数字或字母递增或递减序列",
    "password.pepeatLetter,count": "密码中允许同一字符连续出现的次数",
    "password.phoneOrEmail.enable": "密码中不能包含用户手机号码或邮箱账号",
    "password.historySame.time": "密码不能与历史密码重复时长",
    "password.maxUse.time": "密码不予使用最长天数",
    "password.sameWord": "密码不能包含字典中的词汇",
    "password.needFirstUpdate": "首次登录是否需要修改密码",

    "ipAddress.config": "IP策略配置",
    "ipAddress.start": "开始IP",
    "ipAddress.end": "结束IP",
  }
}