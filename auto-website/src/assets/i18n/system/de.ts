export default {
  system: {
    "overview": "System Übersicht",
    "auth.manage": "Behörde Verwaltung",
    "log.manage": "Journal Verwaltung",
    "role.manage": "Rolle Verwaltung",
    "user.manage": "Benutzer Verwaltung",
    "log.config": "Journal Zuteilung",
    "log.records": "Journal Aufzeichnung",
    "scheduledManage": "Zeitliche Aufgabenverwaltung",

    "configRole": "Rolle konfigurieren",
    "authAll": "Alle Berechtigungen",
    "authChecked": "Ausgewählte Berechtigungen",

    "configUser": "Benutzer konfigurieren",
    "roleAll": "Alle Rollen",
    "roleChecked": "Ausgewählte Rolle",

    "safetyPolicy": "Sicherheit Strategie",
    "accountPolicy": "Konto Strategie",
    "passwordPolicy": "Passwort Strategie",
    "ipPolicy": "IP Adresse Strategie",
    "timePolicy": "Login-Zeit Strategie",

    "account.minLen": "Mindestzeichenanzahl für das Konto",
    "account.expireTime": "Erinnerung vor Kontoablauf (Tage)",

    "password.maxLen": "Maximale Anzahl Zeichen",
    "password.minLen": "Mindestanzahl Zeichen",
    "password.minLetter": "Mindestanzahl Buchstaben",
    "password.minUpperLetter": "Mindestanzahl Großbuchstaben",
    "password.minLowerLetter": "Mindestanzahl Kleinbuchstaben",
    "password.minSpecialLetter": "Mindestanzahl Sonderzeichen",
    "password.increactLetter.enable": "Das Passwort darf keine auf-oder absteigende Folge von Zahlen oder Buchstaben sein",
    "password.pepeatLetter,count": "Die Anzahl der aufeinanderfolgenden Vorkommen desselben Zeichens, die in einem Passwort zulässig sind",
    "password.phoneOrEmail.enable": "Das Passwort darf weder die Mobiltelefonnummer noch das E-Mail-Konto des Benutzers enthalten",
    "password.historySame.time": "Das Passwort darf nicht mit dem historischen Passwort identisch sein",
    "password.maxUse.time": "Maximale Anzahl an Tagen, die ein Passwort nicht verwendet werden kann",
    "password.sameWord": "Das Passwort darf keine Wörter aus dem Wörterbuch enthalten",
    "password.needFirstUpdate": "Müssen Sie Ihr Passwort ändern, wenn Sie sich zum ersten Mal anmelden",

    "ipAddress.config": "Konfiguration der IP-Richtlinie",
    "ipAddress.start": "IP starten",
    "ipAddress.end": "End-IP",
  }
}