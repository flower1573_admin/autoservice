export const _menuList = [
    {
        path: "/system-manage/log-manage",
        title: "日志管理",
        children: [
            {
                path: "/system-manage/log-manage/log-records",
                title: "操作日志",
            },
            {
                path: "/system-manage/log-manage/audit-log",
                title: "审计日志",
            }
        ]
    },
    {
        path: "/system-manage/scheduled-manage",
        title: "定时任务",
        children: []
    }
]

export default _menuList