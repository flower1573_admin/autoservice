import {h, ref, createApp} from "vue";
import myForm from './index.vue';
import {setupLang} from "../../plugins/lang"
import setupElementPlus from "@/plugins/element-plus";

export default function showHightRisk(options?: any) {
  console.log(options)
  return new Promise((resolve, reject) => {
    const modalRef = ref();
    const mountNode = document.createElement('div');
    document.body.appendChild(mountNode);
    const instanceApp = createApp({
      render() {
        return h(myForm, {
          ref: modalRef,
          options: options,
          onSubmit: (data) => {
            resolve(data);
          },
          onClosed: () => {
            setTimeout(() => {
              mountNode.remove();
            }, 500);
            reject();
          }
        })
      }
    });
    setupLang(instanceApp);
    setupElementPlus(instanceApp)
    instanceApp.mount(mountNode);
    modalRef.value.showModal();
  })
}
