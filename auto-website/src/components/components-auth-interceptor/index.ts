import {h, ref, createApp} from "vue";
import myForm from './index.vue';
import {setupLang} from "../../plugins/lang"
import setupElementPlus from "@/plugins/element-plus";

let dialogCount = 0

export default function showHightRisk(options?: any) {
  if (dialogCount == 0) {
    dialogCount = 1
    return new Promise((resolve, reject) => {
      const modalRef = ref();
      const mountNode = document.createElement('div');
      document.body.appendChild(mountNode);
      const instanceApp = createApp({
        render() {
          return h(myForm, {
            ref: modalRef,
            options: options,
            onSubmit: (data) => {
              resolve(data);
              dialogCount = 0
            },
            onClosed: () => {
              setTimeout(() => {
                mountNode.remove();
                dialogCount = 0
              }, 100);
              reject();
            }
          })
        }
      });
      setupLang(instanceApp);
      setupElementPlus(instanceApp)
      instanceApp.mount(mountNode);
      modalRef.value.showModal();
    })
  } else {
    return new Promise((resolve, reject) => {

    })
  }
}
